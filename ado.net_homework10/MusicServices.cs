﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ado.net_homework10
{
    public class MusicServices
    {
        public void AddBand(string bandName)
        {
            using (var context = new MusicContext())
            {
                Band band = new Band();

                band.Name = bandName;
                context.Bands.Add(band);
                context.SaveChanges();
            }
        }

        public void AddSong(Band band)
        {
            using (var context = new MusicContext())
            {
                Song song = new Song();
                song.BandId = band.Id;
                Console.WriteLine("Enter song name");
                song.Name = Console.ReadLine().ToLower();

                while (true)
                {
                    string playTime = "";
                    Console.WriteLine("Enter song play time");
                    playTime = Console.ReadLine().ToLower();
                    if (Regex.Match(playTime, "^[0-9]*[,][0-9]*$").Success)
                    {
                        song.PlayTime = double.Parse(playTime);
                        break;
                    }
                    else Console.WriteLine("Must be float value in correct format, exapmle: 1,0");

                }

                while (true)
                {
                    string rank = "";
                    Console.WriteLine("Enter song rank");
                    rank = Console.ReadLine().ToLower();
                    if (Regex.Match(rank, "^[0-5]*$").Success)
                    {
                        song.Rank = int.Parse(rank);
                        break;
                    }
                    else Console.WriteLine("Rank must be numeric value between 0 to 5");

                }
                context.Songs.Add(song);
                context.SaveChanges();
            }
        }

        public Band IfExistBand(string bandName)
        {
            bandName.ToLower();
            using (var context = new MusicContext())
            {
                var bandsContext = context.Bands.Where(band => band.Name == bandName).ToList();
                if (bandsContext.FirstOrDefault() != null)
                {
                    return bandsContext[0];
                }
                return null;
            }

        }

        public List<SongsBandsAsset> FindSongs(string songName = "")
        {
            using (var context = new MusicContext())
            {

                var listAllSongs = from song in context.Songs
                                   join band in context.Bands on song.BandId equals band.Id
                                   select new SongsBandsAsset { SongName = song.Name, SongPlayTime = song.PlayTime, SongRank = song.Rank, BandName = band.Name };

                if (songName == "")
                {
                    return listAllSongs.ToList();
                }
                else
                {
                    var foundSongs = listAllSongs.Where(song => song.SongName == songName);
                    return foundSongs.ToList();
                }
            }
        }

        public List<SongsBandsAsset> FindBand(string bandName)
        {
            using (var context = new MusicContext())
            {
                var listAllBands = from band in context.Bands
                                   join song in context.Songs on band.Id equals song.BandId
                                   select new SongsBandsAsset { SongName = song.Name, SongPlayTime = song.PlayTime, SongRank = song.Rank, BandName = band.Name };


                var foundBand = listAllBands.Where(band => band.BandName == bandName);
                return foundBand.ToList();
            }

        }
    }
}
