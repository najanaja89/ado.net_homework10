﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework10
{
    class Program
    {
        static void Main(string[] args)
        {
            #region CreateTables
            //using (var contex = new MusicContext())
            //{
            //    Band band = new Band
            //    {
            //        Name = "TestBand1"
            //    };

            //    Song song = new Song
            //    {
            //        Name = "TestSong",
            //        PlayTime = 5.6,
            //        Rank = 4,
            //        BandId = band.Id
            //    };
            //    contex.Songs.Add(song);
            //    contex.Bands.Add(band);
            //    contex.SaveChanges();
            //}
            #endregion

            MusicServices musicServices = new MusicServices();
            while (true)
            {
                string menu = "";
                Console.WriteLine("Enter 1 to add new Band");
                Console.WriteLine("Enter 2 to add new Song");
                Console.WriteLine("Enter 3 to List all Songs");
                Console.WriteLine("Enter 4 to find Song");
                Console.WriteLine("Enter 5 to find Band");
                //Console.WriteLine("Enter 4 to List all Songs by Rank - to high");
                Console.WriteLine("Enter 0 to exit");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        string newBandName = "";
                        Console.WriteLine("Enter new band name");
                        newBandName = Console.ReadLine().ToLower();
                        if (musicServices.IfExistBand(newBandName) == null)
                        {
                            musicServices.AddBand(newBandName);
                            break;
                        }
                        else Console.WriteLine("Band exist");
                        break;

                    case "2":

                        string bandName = "";
                        Console.WriteLine("Enter band name");
                        bandName = Console.ReadLine().ToLower();
                        if (musicServices.IfExistBand(bandName) != null)
                        {
                            musicServices.AddSong(musicServices.IfExistBand(bandName));
                        }
                        else Console.WriteLine("Band not exist");
                        break;

                    case "3":
                        string subMenu = "";
                        Console.WriteLine("Press 1 to sort songs by Rank to lower ");
                        Console.WriteLine("Press 2 to sort songs by Rank to higher ");
                        subMenu = Console.ReadLine();
                        switch (subMenu)
                        {
                            case "1":
                                var songsList1 = musicServices.FindSongs().OrderBy(song => song.SongRank);
                                Console.WriteLine();
                                Console.WriteLine("=================================================================================================");
                                foreach (var item in songsList1)
                                {
                                    Console.WriteLine($"\tSong name: {item.SongName} \tSong Play Time: {item.SongPlayTime} \tSong Rank: {item.SongRank} \tSong Band: {item.BandName}");
                                }
                                Console.WriteLine("=================================================================================================");
                                Console.WriteLine();

                                break;
                            case "2":
                                var songsList2 = musicServices.FindSongs().OrderByDescending(song => song.SongRank);
                                Console.WriteLine();
                                Console.WriteLine("=================================================================================================");
                                foreach (var item in songsList2)
                                {
                                    Console.WriteLine($"\tSong name: {item.SongName} \tSong Play Time: {item.SongPlayTime} \tSong Rank: {item.SongRank} \tSong Band: {item.BandName}");
                                }
                                Console.WriteLine("=================================================================================================");
                                Console.WriteLine();
                                break;
                            default:
                                break;
                        }
                        break;

                    case "4":
                        string foundSong = "";
                        Console.WriteLine("Enter song to find");
                        foundSong = Console.ReadLine().ToLower();
                        var foundSongs = musicServices.FindSongs(foundSong).OrderBy(song => song.SongRank);
                        Console.WriteLine();
                        Console.WriteLine("=================================================================================================");
                        foreach (var item in foundSongs)
                        {
                            Console.WriteLine($"\tSong name: {item.SongName} \tSong Play Time: {item.SongPlayTime} \tSong Rank: {item.SongRank} \tSong Band: {item.BandName}");
                        }
                        Console.WriteLine("=================================================================================================");
                        Console.WriteLine();
                        break;

                    case "5":
                        string foundBand = "";
                        Console.WriteLine("Enter Band to find");
                        foundBand = Console.ReadLine().ToLower();
                        var foundBands = musicServices.FindBand(foundBand).OrderBy(song => song.SongRank);
                        Console.WriteLine();
                        Console.WriteLine("=================================================================================================");
                        foreach (var item in foundBands)
                        {
                            Console.WriteLine($"\tBand name: {item.BandName}  \tSongs: {item.SongName} \tSong Play Time: {item.SongPlayTime} \tSong Rank: {item.SongRank}");
                        }
                        Console.WriteLine("=================================================================================================");
                        Console.WriteLine();
                        break;

                    case "0":
                        System.Environment.Exit(0);
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
