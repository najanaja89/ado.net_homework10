﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ado.net_homework10
{
    public class Song
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public double PlayTime { get; set; }
        public int Rank { get; set; }
        public virtual Band Band { get; set; }
        public Guid BandId { get; set; }

    }
}