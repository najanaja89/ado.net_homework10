﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework10
{
    public class SongsBandsAsset
    {
        public string SongName { get; set; }
        public string BandName { get; set; }
        public double SongPlayTime { get; set; }
        public int SongRank { get; set; }
    }
}
